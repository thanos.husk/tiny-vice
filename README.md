# Tiny vice 

## Description
This vice was a weekend machining project, designed based upon multiple COTS PCB vices available. (Stickvice etc)    
in order to be used at the [Hackerspace.gr](https://www.hackerspace.gr/) to hold PCB's and other parts in general.  
It is a bit overkill for the application, however it was fun to make and is a good intro project to understand press fits / sliding fits and machining tolerances in general.    

## Photos
![Tiny_vice_V2_tall_1](/uploads/e6a736f7b57ce67847032ba49f211b91/Tiny_vice_V2_tall_1.jpg)
![Tiny_vice_V2_shotrt](/uploads/5a1b44c76aafd3676c9b427a09c4e07a/Tiny_vice_V2_shotrt.jpg)
![Tiny_vice_V2_tall_2](/uploads/3626314967812f5153f0b5896f203e76/Tiny_vice_V2_tall_2.jpg)
## License
This project is licensed under the [CERN OHL v1.2](https://gitlab.com/thanos.husk/tiny-vice/-/blob/main/LICENSE) 
